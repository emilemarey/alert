package com.loni.alert.producer;

import com.loni.alert.common.CommonData;
import com.loni.alert.entities.TaskEntity;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

class PushNotificationProducerTest extends CommonData {

    @InjectMocks
    private PushNotificationProducer pushNotificationProducer;

    @Mock
    private KafkaTemplate kafkaTemplate;

    @Value("${topic.push.notification}")
    protected String topicName;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        pushNotificationProducer = new PushNotificationProducer(kafkaTemplate);
        pushNotificationProducer.topicName = topicName;
    }

    @AfterEach
    public void tearDown() {
        pushNotificationProducer = null;
    }

    @Test
    void sendPushNotification() {
        pushNotificationProducer.sendPushNotification(taskEntityUp());
        verify(kafkaTemplate, atLeastOnce()).send(topicName, taskEntityUp().toString());
    }
}