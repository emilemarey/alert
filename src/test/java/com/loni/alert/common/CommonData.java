package com.loni.alert.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loni.alert.entities.DeviceEntity;
import com.loni.alert.entities.TaskEntity;

import java.math.BigInteger;

public class CommonData {

    private ObjectMapper mapper = new ObjectMapper();

    public TaskEntity taskEntityAlerted() {
        return new TaskEntity("idDispositivo", "alerted");
    }

    public TaskEntity taskEntityUp() {
        return new TaskEntity("idDispositivo", "up");
    }

    public TaskEntity taskEntityDown() {
        return new TaskEntity("idDispositivo", "down");
    }

    public DeviceEntity deviceEntity() {
        return new DeviceEntity(new BigInteger("1"),"idDispositivo", "dispositivo_name","down");
    }

    public String taskEntityToString(TaskEntity taskEntity) {
        try {
            return mapper.writeValueAsString(taskEntity);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
