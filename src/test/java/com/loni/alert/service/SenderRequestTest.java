package com.loni.alert.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loni.alert.common.CommonData;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class SenderRequestTest extends CommonData {

    @InjectMocks
    private SenderRequest senderRequest;
    @Mock
    private ObjectMapper objectMapper;
    @Mock
    private RestTemplate restTemplate;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        senderRequest = new SenderRequest(objectMapper);
        senderRequest.restTemplate=restTemplate;
    }

    @AfterEach
    public void tearDown() {
        senderRequest = null;
        objectMapper = null;
        restTemplate = null;
    }

    @Test
    void sendRequestPost() throws JsonProcessingException {
        when(objectMapper.writeValueAsString(ArgumentMatchers.any())).thenReturn(taskEntityToString(taskEntityAlerted()));
        when(restTemplate.postForEntity(any(), any(), any()))
                .thenReturn(
                        new ResponseEntity<>(
                                taskEntityToString(taskEntityAlerted()), HttpStatus.OK));
        ResponseEntity<String> response = senderRequest.sendRequestPost(taskEntityAlerted());
        verify(objectMapper, atLeastOnce()).writeValueAsString(any());
        verify(restTemplate, atLeastOnce()).postForEntity(any(), any(), any());
        assertTrue(HttpStatus.OK.value()==response.getStatusCodeValue());
        assertTrue(taskEntityToString(taskEntityAlerted()).equals(response.getBody()));

    }
}