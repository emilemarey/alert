package com.loni.alert.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loni.alert.common.CommonData;
import com.loni.alert.entities.DeviceEntity;
import com.loni.alert.entities.TaskEntity;
import com.loni.alert.producer.PushNotificationProducer;
import com.loni.alert.repositories.DeviceRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class CreatorMessageToSendServiceTest extends CommonData {

    @InjectMocks
    private CreatorMessageToSendService creatorMessageToSendService;

    @Mock
    private PushNotificationProducer pushNotificationProducer;

    @Mock
    private ObjectMapper objectMapper;

    @Mock
    private DeviceRepository deviceRepository;

    @Mock
    private SenderRequest sendRequest;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        creatorMessageToSendService = new CreatorMessageToSendService(pushNotificationProducer,
                objectMapper, deviceRepository, sendRequest);
    }

    @AfterEach
    public void tearDown() {
        creatorMessageToSendService = null;
        pushNotificationProducer = null;
        objectMapper = null;
        deviceRepository = null;
    }

    @Test
    void sendPushNotificationAlertedWhenDeviceHasStatusDown() throws JsonProcessingException {

        String taskAlerted = taskEntityToString(taskEntityAlerted());
        when(objectMapper.readValue(anyString(), eq(TaskEntity.class))).thenReturn(taskEntityAlerted());

        when(deviceRepository.findOneByDeviceId(taskEntityAlerted().getId())).thenReturn(deviceEntity());

        when(deviceRepository.save(any(DeviceEntity.class))).thenReturn(deviceEntity());

        when(sendRequest.sendRequestPost(any()))
                .thenReturn(new ResponseEntity(taskEntityAlerted(), HttpStatus.OK));

        creatorMessageToSendService.sendPushNotification(taskAlerted);

        verify(objectMapper, times(1)).readValue(anyString(), eq(TaskEntity.class));
        verify(deviceRepository, times(1)).findOneByDeviceId(anyString());
        verify(deviceRepository, times(1)).save(any());
        verify(sendRequest, atLeastOnce()).sendRequestPost(any());
        verify(pushNotificationProducer, atLeastOnce()).sendPushNotification(any());

    }

    @Test
    void sendPushNotificationUpWhenDeviceHasStatusDown() throws JsonProcessingException {

        String taskAlerted = taskEntityToString(taskEntityUp());
        when(objectMapper.readValue(anyString(), eq(TaskEntity.class))).thenReturn(taskEntityUp());

        when(deviceRepository.findOneByDeviceId(taskEntityUp().getId())).thenReturn(deviceEntity());

        when(deviceRepository.save(any(DeviceEntity.class))).thenReturn(deviceEntity());

        when(sendRequest.sendRequestPost(any()))
                .thenReturn(new ResponseEntity(taskEntityUp(), HttpStatus.OK));

        creatorMessageToSendService.sendPushNotification(taskAlerted);

        verify(objectMapper, times(1)).readValue(anyString(), eq(TaskEntity.class));
        verify(deviceRepository, times(1)).findOneByDeviceId(anyString());
        verify(deviceRepository, times(1)).save(any());
        verify(sendRequest, atLeastOnce()).sendRequestPost(any());
        verify(pushNotificationProducer, atLeastOnce()).sendPushNotification(any());

    }


    @Test
    void sendPushNotificationDownWhenDeviceHasStatusDown() throws JsonProcessingException {

        String taskAlerted = taskEntityToString(taskEntityDown());
        when(objectMapper.readValue(anyString(), eq(TaskEntity.class))).thenReturn(taskEntityDown());

        when(deviceRepository.findOneByDeviceId(taskEntityDown().getId())).thenReturn(deviceEntity());

        when(deviceRepository.save(any(DeviceEntity.class))).thenReturn(deviceEntity());

        when(sendRequest.sendRequestPost(any()))
                .thenReturn(new ResponseEntity(taskEntityAlerted(), HttpStatus.OK));

        creatorMessageToSendService.sendPushNotification(taskAlerted);

        verify(objectMapper, times(1)).readValue(anyString(), eq(TaskEntity.class));
        verify(deviceRepository, times(1)).findOneByDeviceId(anyString());
    }

}