package com.loni.alert.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.loni.alert.common.CommonData;
import com.loni.alert.service.CreatorMessageToSendService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

class TaskConsumerTest extends CommonData {

    @InjectMocks
    private TaskConsumer taskConsumer;

    @Mock
    private ObjectMapper objectMapper;

    @Mock
    private CreatorMessageToSendService creatorMessageToSendService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        taskConsumer = new TaskConsumer(objectMapper, creatorMessageToSendService);
    }

    @AfterEach
    public void tearDown() {
        taskConsumer.resetLatch();
        taskConsumer = null;
        objectMapper = null;
        creatorMessageToSendService = null;
    }

    @Test
    void consume() {
        taskConsumer.consume(taskEntityToString(taskEntityDown()));
        verify(creatorMessageToSendService, atLeastOnce()).sendPushNotification(taskEntityToString(taskEntityDown()));
        assertTrue(taskConsumer.getPayload().equals(taskEntityToString(taskEntityDown())));
    }
}