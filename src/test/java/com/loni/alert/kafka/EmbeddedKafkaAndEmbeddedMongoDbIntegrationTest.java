package com.loni.alert.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.loni.alert.AlertApplication;
import com.loni.alert.consumer.TaskConsumer;
import com.loni.alert.entities.DeviceEntity;
import com.loni.alert.entities.TaskEntity;
import com.loni.alert.producer.TaskNotificationProducer;
import com.loni.alert.repositories.DeviceRepository;
import com.loni.alert.service.SetupEnvironment;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(classes = AlertApplication.class)
@DirtiesContext
@EmbeddedKafka(partitions = 1, brokerProperties = { "listeners=PLAINTEXT://localhost:9092", "port=9092" })
@TestPropertySource(properties = "spring.mongodb.embedded.version=3.5.5")
class EmbeddedKafkaAndEmbeddedMongoDbIntegrationTest {
    @Autowired
    private SetupEnvironment setupEnvironment;

    @Autowired
    DeviceRepository deviceRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TaskConsumer consumer;

    @Autowired
    private TaskNotificationProducer producer;

    @Test
    public void givenEmbeddedKafkaBroker_whenSendingWithSimpleProducer_thenMessageReceived()
            throws Exception {
        //This is setup to create the device with status up
        setupEnvironment.cleanAndCreateDeviceEntity();

        TaskEntity taskEntity = new TaskEntity("deviceId", "down");

        String taskString = objectMapper.writeValueAsString(taskEntity);
        producer.sendTask(taskString);

        boolean messageConsumed = consumer.getLatch().await(10, TimeUnit.SECONDS);
        assertTrue(messageConsumed);
        assertTrue(consumer.getPayload().equals(taskString));

        DeviceEntity deviceEntity = deviceRepository.findOneByDeviceId(taskEntity.getId());
        assertNotNull(deviceEntity, "Device doesn't exists in database");

        assertTrue(deviceEntity.getStatus().equals(taskEntity.getStatus()), "The device wasn't updated correctly");
    }
}