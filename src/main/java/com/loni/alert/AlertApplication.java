package com.loni.alert;

/*import com.fasterxml.jackson.databind.ObjectMapper;
import com.loni.alert.entities.DeviceEntity;
import com.loni.alert.entities.TaskEntity;
import com.loni.alert.producer.TaskNotificationProducer;
import com.loni.alert.repositories.DeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;*/
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlertApplication {
//public class AlertApplication implements CommandLineRunner {

/*	@Autowired
	private TaskNotificationProducer taskNotificationProducer;

	@Autowired
	private ObjectMapper objectMapper;
*/

	public static void main(String[] args) {
		SpringApplication.run(AlertApplication.class, args);
	}

/*	@Override
	public void run(String... args) throws Exception {
		System.out.println("************ Sending push notification ********");

		TaskEntity taskEntity = new TaskEntity();
		taskEntity.setId("otrodispositivo");
		taskEntity.setStatus("up");
		taskNotificationProducer.sendTask(
				objectMapper.writeValueAsString(
						taskEntity));
	}*/

}
