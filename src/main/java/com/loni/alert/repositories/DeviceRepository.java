package com.loni.alert.repositories;

import com.loni.alert.entities.DeviceEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeviceRepository extends MongoRepository<DeviceEntity, String> {

    DeviceEntity  findOneByDeviceId(String deviceId);

}
