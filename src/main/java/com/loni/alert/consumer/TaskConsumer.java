package com.loni.alert.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.loni.alert.service.CreatorMessageToSendService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.concurrent.CountDownLatch;

@Service
@Slf4j
public class TaskConsumer {

    private ObjectMapper objectMapper;

    private CreatorMessageToSendService creatorMessageToSendService;

    private CountDownLatch latch = new CountDownLatch(1);
    private String payload;


    public TaskConsumer(ObjectMapper objectMapper, CreatorMessageToSendService creatorMessageToSendService) {
        this.objectMapper = objectMapper;
        this.creatorMessageToSendService = creatorMessageToSendService;
    }

    @KafkaListener(topics = "${topic.task.notification}")
    public void consume(String json) {
        if(null == json){
            String error = "The Json object is null";
            log.error(error);
            throw new NullPointerException(error);
        }

        log.info("######################### Receiving push notification: " + json);
        setPayload(json);
        creatorMessageToSendService.sendPushNotification(json);
        latch.countDown();
    }

    public void resetLatch() {
        latch = new CountDownLatch(1);
    }

    public CountDownLatch getLatch() {
        return latch;
    }

    public String getPayload() {
        return payload;
    }

    private void setPayload(String payload) {
        this.payload = payload;
    }
}
