package com.loni.alert.service;

import com.loni.alert.entities.DeviceEntity;
import com.loni.alert.repositories.DeviceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SetupEnvironment {

    private DeviceRepository deviceRepository;

    public SetupEnvironment(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    //This is to set the environment to test it
    public void cleanAndCreateDeviceEntity() {
        deviceRepository.deleteAll();
        log.info("########## All Device were removed");
        DeviceEntity deviceEntity = new DeviceEntity(null, "deviceId", "deviceName", "up");
        deviceRepository.save(deviceEntity);
        log.info("########## Device entity was created: " + deviceEntity.toString() );
    }

}
