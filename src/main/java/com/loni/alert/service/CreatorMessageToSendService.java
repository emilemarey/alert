package com.loni.alert.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loni.alert.entities.DeviceEntity;
import com.loni.alert.entities.TaskEntity;
import com.loni.alert.producer.PushNotificationProducer;
import com.loni.alert.repositories.DeviceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class CreatorMessageToSendService {

    private PushNotificationProducer pushNotificationProducer;
    private ObjectMapper objectMapper;
    private DeviceRepository deviceRepository;
    private SenderRequest sendRequest;

    public CreatorMessageToSendService(PushNotificationProducer pushNotificationProducer, ObjectMapper objectMapper,
                                       DeviceRepository deviceRepository, SenderRequest sendRequest) {

        this.pushNotificationProducer = pushNotificationProducer;
        this.objectMapper = objectMapper;
        this.deviceRepository = deviceRepository;
        this.sendRequest = sendRequest;

    }

    public void sendPushNotification(String jsonToSend) {

        try {
            TaskEntity taskEntity = objectMapper.readValue(jsonToSend, TaskEntity.class);

            DeviceEntity deviceEntity = deviceRepository.findOneByDeviceId(taskEntity.getId());

            //validate the status value to device and check if we need to send the push notification and call the post
            if (!taskEntity.getStatus().equalsIgnoreCase(deviceEntity.getStatus())) {

                log.info("############## Sending push......");
                deviceEntity.setStatus(taskEntity.getStatus());
                deviceRepository.save(deviceEntity);
                sendRequest.sendRequestPost(taskEntity);
                pushNotificationProducer.sendPushNotification(taskEntity);

            }
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

}
