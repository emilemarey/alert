package com.loni.alert.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loni.alert.entities.TaskEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@Service
@Slf4j
public class SenderRequest {

    private ObjectMapper objectMapper;

    protected RestTemplate restTemplate;

    @Value("${url.to.send.post}")
    private String URL;

    public SenderRequest(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
        this.restTemplate = new RestTemplate();
    }

    protected ResponseEntity<String> sendRequestPost(TaskEntity taskEntity) {

        log.info("################ sendRequestPost: " + taskEntity.toString());
        URI uri;
        ResponseEntity<String> response = null;

        try {
            uri = new URI(URL +"/"+taskEntity.getId());

            HttpHeaders headers = new HttpHeaders();
            headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);

            String json = objectMapper.writeValueAsString(taskEntity);

            HttpEntity<String> request = new HttpEntity<>(json, headers);

            response = restTemplate.postForEntity(uri, request, String.class);

        } catch (URISyntaxException | JsonProcessingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }

        return response;
    }
}
