package com.loni.alert.entities;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.math.BigInteger;

@Document("device")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DeviceEntity implements Serializable {
    @Id
    @Indexed(unique = true)
    private BigInteger id;

    @Indexed(unique = true)
    private String deviceId;
    private String deviceName;
    private String status;

}
