package com.loni.alert.entities;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TaskEntity {

    private String id;
    private String status;

}
