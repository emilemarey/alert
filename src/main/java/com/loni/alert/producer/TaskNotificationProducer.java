package com.loni.alert.producer;

import com.loni.alert.service.SetupEnvironment;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class TaskNotificationProducer {

    @Value("${topic.task.notification}")
    private String topicName;

    private SetupEnvironment setupEnvironment;
    private KafkaTemplate<String, String> kafkaTemplate;

    public TaskNotificationProducer(SetupEnvironment setupEnvironment, KafkaTemplate<String, String> kafkaTemplate) {
        this.setupEnvironment = setupEnvironment;
        this.kafkaTemplate = kafkaTemplate;
    }

    public void sendTask(String task) {
        setupEnvironment.cleanAndCreateDeviceEntity();
        kafkaTemplate.send(topicName, task);
    }
}
