package com.loni.alert.producer;

import com.loni.alert.entities.TaskEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PushNotificationProducer {

    @Value("${topic.push.notification}")
    protected String topicName;

    private KafkaTemplate<String, String> kafkaTemplate;

    public PushNotificationProducer(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void sendPushNotification(TaskEntity taskEntity) {
        log.info("########## sending push notification to kafka");
        kafkaTemplate.send(topicName, taskEntity.toString());

    }
}
