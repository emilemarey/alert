I've created a docker compose it contain kafka and mongodb

# create topic task-topic
kafka-topics.sh --bootstrap-server localhost:9092 --create --topic task-topic --partitions 1 --replication-factor 1

# list topic
kafka-topics.sh --bootstrap-server localhost:9092 --list

# describe topic
kafka-topics.sh --bootstrap-server localhost:9092 --describe --topic task-topic

# delete topic task-topic
kafka-topics.sh --bootstrap-server localhost:9092 --delete --topic task-topic